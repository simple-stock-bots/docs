# Simple Stock Bots
[![Platform](https://img.shields.io/badge/platform-Telegram-blue.svg)](https://t.me/SimpleStockBot)
[![Platform](https://img.shields.io/badge/platform-Discord-blue.svg)](https://discordapp.com/api/oauth2/authorize?client_id=532045200823025666&permissions=36507338752&scope=bot)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)
 

## Table of Contents
+ [About](#about)
+ [Getting Started](#getting_started)
+ [Usage](#usage)
+ [Contributing](../CONTRIBUTING.md)

## About <a name = "about"></a>
Simple Stock Bots is a family of messaging bots that aim to provide simple bots that provide information on the stock market. Currently there are only [Telegram](./telegram) and [Discord](./discord) Bots.

## Getting Started <a name = "getting_started"></a>

The simplest way to use these bots is to use the version that I host on Digital Ocean. You can find the links to access them below:

+ [Telegram](https://t.me/SimpleStockBot)
+ [Discord](https://discordapp.com/api/oauth2/authorize?client_id=532045200823025666&permissions=36507338752&scope=bot)

Alternatively, this project is completely open source and instructions on how to run your own instance of the bot are provided on each of the bots pages. 

## Source Code

The source code for the bots is completely open and hosted on Gitlab. 

+ [Telegram](https://gitlab.com/simple-stock-bots/simple-telegram-bot)
+ [Discord](https://gitlab.com/simple-stock-bots/simple-discord-stock-bot)
