Simple Stock Bot is run entirely on donations, and costs about $420 a year to run. All donations go directly towards paying for servers, and premium market data provided by [matketdata.app](https://dashboard.marketdata.app/marketdata/aff/go/misterbiggs?keyword=repo).

The best way to donate is through [Buy Me A Coffee](https://www.buymeacoffee.com/Anson) which accepts Paypal or Credit card.

Alternatively, you can donate on Telegram with the `/donate` [command](commands.md#donate-amount-in-usd).

If you have any questions get in [touch.](contact.md)

## Other Ways to Help:

- Follow me on [twitter](https://twitter.com/AnsonBiggs)
- Contribute to the project on [GitLab](https://gitlab.com/simple-stock-bots) or just leave a star
- Using my referral links to host your own Bot
  - [matketdata.app](https://dashboard.marketdata.app/marketdata/aff/go/misterbiggs?keyword=repo)
  - [DigitalOcean](https://m.do.co/c/6b5df7ef55b6)