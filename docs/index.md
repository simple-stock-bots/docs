# Simple Stock Bots

Simple Stock Bots is a family of messaging bots that aim to provide simple interface to stock market information.

## Getting Started

The bots can either be added to a server or can be direct messaged for the exact same functionality. Links for each platform are below:

[Telegram Bot :fontawesome-brands-telegram:](https://t.me/SimpleStockBot){ .md-button .md-button--primary }

[Discord Bot :fontawesome-brands-discord:](https://discordapp.com/api/oauth2/authorize?client_id=532045200823025666&permissions=36507338752&scope=bot){ .md-button .md-button--primary }

Alternatively, this project is completely open source and instructions on how to run your own instance of the bot are provided on each of the bots pages. [Hosting](host.md)

## Usage

The bots are nearly identical between platforms, but sometimes a platform will have a feature that only it supports such as Telegrams inline messaging or Discords embeds. For full information see [Commands](commands.md)

## Source Code

The source code for the bots is completely open and hosted on GitLab. The project is licensed with an MIT License which is incredibly permissive. [The full text of the license can be read here](./LICENSE)

- [Telegram Bot Source](https://gitlab.com/simple-stock-bots/simple-telegram-stock-bot)
- [Discord Bot Source](https://gitlab.com/simple-stock-bots/simple-discord-stock-bot)

If you decide to host your own bots using my source code please consider using my referral links for marketdata.app and DigitalOcean. We both get a kickback when you use them.

- [DigitalOcean](https://m.do.co/c/6b5df7ef55b6)
- [matketdata.app](https://dashboard.marketdata.app/marketdata/aff/go/misterbiggs?keyword=repo)
