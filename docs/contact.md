Feel free to reach out with bugs or suggestions at any of the following places:

- Message me on [Telegram](https://t.me/MisterBiggs)
- Message me on Discord `MisterBiggs#0465`
- Tweet me on [Twitter](https://twitter.com/AnsonBiggs)
- Open an issue on gitlab: [`Telegram Bot`](https://gitlab.com/simple-stock-bots/simple-telegram-stock-bot) [`Discord Bot`](https://gitlab.com/simple-stock-bots/simple-discord-stock-bot)